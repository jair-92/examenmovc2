package com.example.examenmovc2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.examenmovc2.Ventas;

public class MainActivity extends AppCompatActivity {

    private EditText txtNumBomba, txtPrecio, txtCapacidadBomba, txtContadorLitros, txtCantidad;
    private RadioButton radioButtonRegular, radioButtonExtra;
    private Button btnIniciarBomba, btnHacerVenta, btnRegistrarVenta, btnConsultarVentas, btnLimpiar, btnSalir;
    private TextView lblVentaDetalle;

    private BombaGasolina bombaGasolina;
    private VentaDb ventaDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtener las referencias de los elementos del layout
        txtNumBomba = findViewById(R.id.txtNumBomba);
        txtPrecio = findViewById(R.id.txtPrecio);
        txtCapacidadBomba = findViewById(R.id.txtCapacidadBomba);
        txtContadorLitros = findViewById(R.id.txtContadorLitros);
        txtCantidad = findViewById(R.id.txtCantidad);
        radioButtonRegular = findViewById(R.id.radioButtonRegular);
        radioButtonExtra = findViewById(R.id.radioButtonExtra);
        btnIniciarBomba = findViewById(R.id.btnIniciarBomba);
        btnHacerVenta = findViewById(R.id.btnHacerVenta);
        btnRegistrarVenta = findViewById(R.id.btnRegistrarVenta);
        btnConsultarVentas = findViewById(R.id.btnConsultarVentas);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);
        lblVentaDetalle = findViewById(R.id.lblVentaDetalle);

        // Inicializar la base de datos de ventas
        ventaDb = new VentaDb(this);

        // Configurar el evento de clic del botón Iniciar Bomba
        btnIniciarBomba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener los valores ingresados por el usuario
                int numBomba = Integer.parseInt(txtNumBomba.getText().toString());
                double precio = Double.parseDouble(txtPrecio.getText().toString());
                int capacidadBomba = Integer.parseInt(txtCapacidadBomba.getText().toString());
                int contadorLitros = Integer.parseInt(txtContadorLitros.getText().toString());

                // Obtener el tipo de gasolina seleccionado
                String tipoGasolina;
                if (radioButtonRegular.isChecked()) {
                    tipoGasolina = "Regular";
                } else {
                    tipoGasolina = "Extra";
                }

                // Crear el objeto BombaGasolina con los valores iniciales
                bombaGasolina = new BombaGasolina(numBomba, tipoGasolina, contadorLitros, capacidadBomba);

                // Mostrar mensaje de inicio de bomba
                Toast.makeText(MainActivity.this, "Bomba iniciada", Toast.LENGTH_SHORT).show();
            }
        });

        // Configurar el evento de clic del botón Hacer Venta
        btnHacerVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener la cantidad ingresada por el usuario
                int cantidad = Integer.parseInt(txtCantidad.getText().toString());

                // Verificar si hay suficiente inventario
                if (cantidad <= bombaGasolina.getContadorLitros()) {
                    // Calcular el costo y el total a pagar
                    double precio = Double.parseDouble(txtPrecio.getText().toString());
                    double costo = cantidad * precio;
                    double total = costo * (1 + bombaGasolina.getImpuesto());

                    // Actualizar el contador de litros
                    bombaGasolina.restarLitros(cantidad);

                    // Mostrar los detalles de la venta
                    lblVentaDetalle.setText("Cantidad: " + cantidad + "\n" +
                            "Costo: " + costo + "\n" +
                            "Total a pagar: " + total);

                    // Incrementar el contador de ventas
                    bombaGasolina.incrementarContadorVentas();

                    // Mostrar mensaje de venta realizada
                    Toast.makeText(MainActivity.this, "Venta realizada", Toast.LENGTH_SHORT).show();
                } else {
                    // Mostrar mensaje de inventario insuficiente
                    Toast.makeText(MainActivity.this, "Inventario insuficiente", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Configurar el evento de clic del botón Registrar Venta
        btnRegistrarVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener los datos de la venta
                int numBomba = bombaGasolina.getNumBomba();
                String tipoGasolina = bombaGasolina.getTipoGasolina();
                double precio = Double.parseDouble(txtPrecio.getText().toString());
                int cantidad = Integer.parseInt(txtCantidad.getText().toString());

                // Crear el objeto Venta
                Ventas venta = new Ventas(numBomba, tipoGasolina, precio, cantidad);

                // Insertar la venta en la base de datos
                long ventaId = ventaDb.insertVenta(venta);

                if (ventaId != -1) {
                    // Mostrar mensaje de venta registrada
                    Toast.makeText(MainActivity.this, "Venta registrada", Toast.LENGTH_SHORT).show();
                } else {
                    // Mostrar mensaje de error en la inserción
                    Toast.makeText(MainActivity.this, "Error al registrar la venta", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Configurar el evento de clic del botón Consultar Registro de Ventas
        btnConsultarVentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Crear el intent para abrir la actividad de consulta de ventas
                Intent intent = new Intent(MainActivity.this, ConsultarVentasActivity.class);
                // Pasar a través del intent cualquier dato adicional que necesites
                startActivity(intent);
            }
        });

        // Configurar el evento de clic del botón Limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Limpiar los campos de entrada y los detalles de venta
                txtCantidad.setText("");
                lblVentaDetalle.setText("");
            }
        });

        // Configurar el evento de clic del botón Salir
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Mostrar un diálogo de confirmación antes de salir
                showConfirmExitDialog();
            }
        });
    }

    private void showConfirmExitDialog() {
        // Implementa aquí el diálogo de confirmación para salir de la aplicación
    }
}
